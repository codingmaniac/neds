package com.tailf.packages.ned.ios;

import static com.tailf.packages.ned.nedcom.NedString.getMatch;
import static com.tailf.packages.ned.nedcom.NedString.stringQuote;
import static com.tailf.packages.ned.nedcom.NedString.calculateMd5Sum;

import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tailf.conf.ConfBuf;

import com.tailf.ned.NedWorker;

import com.tailf.packages.ned.nedcom.NedComCliBase;
import com.tailf.packages.ned.nedcom.Schema;


public class IOSCliExtensions implements NedComCliBase.ExtensionsHandler {

    protected IOSNedCli owner;
    protected Schema schema;
    private String operRoot;
    private String operList;

    public IOSCliExtensions(IOSNedCli owner) {
        this.owner = owner;
        this.schema = owner.getCurrentSchema();
        this.operRoot = "/ncs:devices/ncs:device{"
            +owner.device_id
            +"}/ncs:ned-settings/ios-op:cisco-ios-oper/locks";
        this.operList = this.operRoot + "{%s}";
    }

    public void initialize() {
        // Nothing to initialize
    }


    /**
     * "list-redeploy" - Redeploy path when 'self' is modified
     * @param
     * @throws Exception
     */
    public void listRedeploy(final NedWorker worker, Schema.CallbackMetaData metaData,
                             Schema.ParserContext parserContext,
                             final int fromT, final int toT) throws Exception {
        final String self = parserContext.getNCSCurrentKP(owner.device_id);
        String[] tokens = metaData.argument.split(" :: ");
        final String path = self + tokens[0];
        traceVerbose(worker, "   path = "+path);

        final String cmd = parserContext.getCurrentLine().trim();
        if (cmd.startsWith("no ")) {
            return;
        }

        if (!owner.maapiExists(worker, fromT, path)) {
            return;
        }
        if (!owner.maapiExists(worker, toT, path)) {
            return;
        }

        String redeploy = owner.maapiGetConfig(worker, toT, path, Integer.parseInt(tokens[1]));
        traceInfo(worker, "transformed => redeployed: "+stringQuote(redeploy));
        parserContext.injectAfter(redeploy);
    }


    /**
     * "if-redeploy" - Redeploy interface target value when 'self' is modified
     * @param
     * @throws Exception
     */
    public void ifRedeploy(final NedWorker worker, Schema.CallbackMetaData metaData, Schema.ParserContext parserContext,
                           final int fromT, final int toT) throws Exception {
        final String self = parserContext.getNCSCurrentKP(owner.device_id);
        final String ifpath = self.substring(0, self.lastIndexOf("}/")+1);
        traceVerbose(worker, "   ifpath = "+ifpath);

        if (!owner.maapi.exists(toT, ifpath)) {
            return;  // Interface is deleted
        }

        // Do not let switchport content changes redeploy anything
        if (self.endsWith("/switchport")) {
            boolean fromExists = owner.maapi.exists(fromT, self);
            boolean toExists = owner.maapi.exists(toT, self);
            if (fromExists && toExists) {
                // interface * /switchport was not toggled
                return;
            }
        }

        traceVerbose(worker, "   argument = "+metaData.argument);

        // metas[0] = <interface target path>
        // metas[1] = <target cmd>
        // metas[2] = any|create|delete
        // metas[3] = [default value]
        final String[] values = metaData.argument.split(" ;; ");
        for (int n = 0; n < values.length; n++) {
            final String[] metas = values[n].split(" :: ");

            // Check if redeploy is disabled for this config
            if (metas[1].startsWith("service-policy ")
                && !owner.nedSettings.getBoolean("auto/if-switchport-sp-redeploy")) {
                continue; // Redeploy of interface service-policy is disabled
            }

            // Check any|create|delete
            if ("create".equals(metas[2]) && parserContext.currentDataContext.isDelete()) {
                continue; // Self is deleted
            }
            if ("delete".equals(metas[2]) && !parserContext.currentDataContext.isDelete()) {
                continue; // Self is created
            }

            // Get from and to values
            String line = " " + metas[1];

            // type empty (starts with *)
            if (metas[0].startsWith("*")) {
                final String path = metas[0].substring(1);
                boolean from = owner.maapiExists(worker, fromT, ifpath+path);
                if (!from) {
                    continue; // Target is not set
                }
                boolean to = owner.maapiExists(worker, toT, ifpath+path);
                if (!to) {
                    continue; // Target is deleted in this trans
                }

            }

            // type string
            else {
                // Read target from-value and to-value
                final String from = owner.maapiGetLeafString(worker, fromT, ifpath+metas[0]);
                if (from == null) {
                    continue; // Target is not set
                }
                final String to = owner.maapiGetLeafString(worker, toT, ifpath+metas[0]);
                if (to == null) {
                    continue; // Target is deleted in this trans
                }
                if (!from.equals(to)) {
                    continue; // Target is modified in this trans
                }
                if (metas.length > 3 && metas[3].equals(to)) {
                    continue; // Target has default value
                }
                line += (" " + to);
            }

            // Redeploy target value after self is modified
            traceInfo(worker, "transformed => redeployed '"+line+"' in "+ifpath);
            parserContext.injectAfter(line);
        }
    }


    /**
     * "if-default" - Default interface when created
     * @param
     * @throws Exception
     */
    public void ifDefault(final NedWorker worker, Schema.CallbackMetaData metaData, Schema.ParserContext parserContext,
                          final int fromT, final int toT) throws Exception {
        final String ifpath = parserContext.getNCSCurrentKP(owner.device_id);
        traceVerbose(worker, "   ifpath = "+ifpath);

        if (owner.maapi.exists(fromT, ifpath)) {
            return;  // Interface not newly created
        }

        // interface * / ip redirects
        if ("true".equals(owner.maapiGetLeafString(worker, toT, ifpath+"/ip/redirects"))) {
            traceInfo(worker, "transformed => injected interface default 'ip redirects'");
            parserContext.injectAfter(" ip redirects");
        }
    }


    /**
     * config-lock-key - this config is locking target config and need to be temporarily removed before edit
     * @param
     * @throws Exception
     */
    public void configLock(final NedWorker worker, Schema.CallbackMetaData metaData, Schema.ParserContext parserContext,
                           final int fromT, final int toT) throws Exception {
        if (owner.isDry) {
            traceVerbose(worker, "   ignored, dry-run");
            return;
        }

        final String cmd = parserContext.getCurrentLine().replace("\r","");
        if (cmd.trim().startsWith("no ")) {
            // Let locks.inject() or show() delete stale lock cache entries
            traceVerbose(worker, "   ignored for "+stringQuote(cmd));
            return;
        }

        final String args = metaData.argument;
        traceVerbose(worker, "   argument = "+stringQuote(args)+" cmd = "+stringQuote(cmd));
        traceDebug2(worker, "   parent deleted = "
                    +(parserContext.getState() == Schema.ParserState.PARENT_CONTEXT_DELETED));

        // Derive oper path
        final String self = parserContext.getNCSCurrentKP(owner.device_id);
        String hash = calculateMd5Sum(self);
        String operPath = String.format(this.operList, hash);

        // create|update config lock in oper cache
        try {
            final String[] metas = args.split(" :: ");
            final String trigger = cmd.replaceFirst(metas[0], metas[1]);
            traceDebug2(worker, "   trigger = "+stringQuote(trigger));

            // Create unlock config
            final String spaces = cmd.replace(cmd.trim(), "");
            String unlock = toEnter(parserContext);
            unlock += (spaces + "no "+cmd.trim()+"\n");
            unlock += toExit(parserContext);
            traceVerbose(worker, "   unlock = "+stringQuote(unlock));

            if (!owner.cdbOper.exists(operPath)) {
                traceInfo(worker, "locks: created config lock for "+shortpath(self));
                owner.cdbOper.create(operPath);
            } else {
                traceInfo(worker, "locks: updated config lock for "+shortpath(self));
            }
            owner.cdbOper.setElem(new ConfBuf(self), operPath+"/path");
            owner.cdbOper.setElem(new ConfBuf(trigger), operPath+"/trigger");
            owner.cdbOper.setElem(new ConfBuf(unlock), operPath+"/unlock");
        } catch (Exception e) {
            owner.logError(worker, "locks: cache Exception ERROR :: "+e.getMessage(), e);
        }
    }


    /**
     * string-multi-transform - remove|add outer quotes around string without dequoting
     * @param <regex>, where <STRING> is the string to strip quotes from
     */
    public void stringMultiTransform(NedWorker worker, Schema.CallbackMetaData metaData,
                                     Schema.ParserContext parserContext) {
        if (owner.isNetsim()) {
            return;
        }
        final String line = parserContext.getCurrentLine();

        // TO_DEVICE
        if (parserContext.parserDirection == Schema.ParserDirection.TO_DEVICE) {
            Pattern p = Pattern.compile(metaData.argument.replace("<STRING>", "(\\\"(.+)\\\")"));
            Matcher m = p.matcher(line);
            if (m.find()) {
                final String newline = line.replace(m.group(1), m.group(2));
                traceVerbose(worker, "   '"+line+"' => '"+newline+"'");
                parserContext.replaceCurrentLine(newline);
            }
        }

        // FROM_DEVICE
        else {
            Pattern p = Pattern.compile(metaData.argument.replace("<STRING>", "(.+)"));
            Matcher m = p.matcher(line);
            if (m.find()) {
                String text = m.group(1);
                if (text.contains("|") && !text.startsWith("\"")) {
                    final String newline = line.replace(m.group(1), "\"" + text + "\"");
                    traceVerbose(worker, "   '"+line+"' => '"+newline+"'");
                    parserContext.replaceCurrentLine(newline);
                }
            }
        }
    }


    /**
     * string-multi-transform - remove|add outer quotes around string without dequoting
     * @param <regex>, where <STRING> is the string to strip quotes from
     */
    public void stringQuoteInput(NedWorker worker, Schema.CallbackMetaData metaData,
                                 Schema.ParserContext parserContext) {
        if (owner.isNetsim()) {
            return;
        }

        String line = parserContext.getCurrentLine();
        Pattern p = Pattern.compile(metaData.argument.replace("<STRING>", "(.+)"));
        Matcher m = p.matcher(line);
        if (m.find()) {
            String text = m.group(1);
            if (text.contains(" ")) {
                traceVerbose(worker, "   quoting '"+text+"' in "+line);
                parserContext.injectImmediate(line.replace(text, "\""+text+"\""));
                parserContext.skipCurrentLine();
            }
        }
    }


    /**
     * "if-addr-move" - pre-inject delete of interface address if added in the same transaction
     * @param
     * @throws Exception
     */
    public void ifAddrMove(final NedWorker worker, Schema.CallbackMetaData metaData, Schema.ParserContext parserContext,
                           final int fromT, final int toT) throws Exception {

        if (!owner.nedSettings.getBoolean("auto/if-address-delete-patch")) {
            return;
        }

        // Assemble parsed lines so far
        StringBuilder sb = new StringBuilder();
        for (String l : parserContext.parsedLines()) {
            sb.append(l+"\n");
        }
        final String data = "\n" + sb.toString();

        // No address is set before this delete
        if (!(data.contains("\n ip address ") || data.contains("\n ipv6 address "))) {
            return;
        }

        String cmd = parserContext.getCurrentLine();
        traceVerbose(worker, "  cmd="+cmd);

        // no interface *
        if (cmd.startsWith("no interface ")) {
            // Interface deleted, always inject shutdown
            String inject = cmd.substring(3)+"\n shutdown\nexit\n";
            if (!data.contains(inject) && owner.extInjectFirst.indexOf(inject) < 0) {
                traceInfo(worker, "transformed => auto/if-address-delete-patch pre-injected "+stringQuote(inject));
                owner.extInjectFirst.append(inject);
            }
            return;
        }

        // ip address
        // no ip address
        else if (" ip address".equals(cmd) || " no ip address".equals(cmd)) {
            final String self = parserContext.getNCSCurrentKP(owner.device_id);
            final String addr = owner.maapiGetLeafString(worker, fromT, self+"/../../address/primary/address");
            traceVerbose(worker, "  addr="+addr);
            if (addr == null || !data.contains("\n ip address "+addr)) {
                return; // Address not set in same trans
            }
            cmd = " no ip address"; // !ip address
        }

        // no ip address A.B.C.D M.M.M.M
        // no ipv6 address X:X:X:X::X
        else if (cmd.startsWith(" no ip")) {
            if (!parserContext.currentDataContext.isDelete()) {
                return; // Address not deleted
            }
            if (!data.contains("\n"+cmd.substring(3))) {
                return; // Address not set in same trans
            }
        } else {
            return;
        }

        // Pre-inject the interface address delete
        final String inject = toEnter(parserContext).trim()+"\n"+cmd+"\nexit\n";
        if (owner.extInjectFirst.indexOf(inject) < 0) {
            traceInfo(worker, "transformed => auto/if-address-delete-patch pre-injected "+stringQuote(inject));
            owner.extInjectFirst.append(inject);
            parserContext.skipCurrentLine();
        }
    }


    /**
     * "if-sp-move" - pre-inject delete of interface service-policy if added in same transaction
     * @param
     * @throws Exception
     */
    public void ifSPMove(final NedWorker worker, Schema.CallbackMetaData metaData, Schema.ParserContext parserContext,
                         final int fromT, final int toT) throws Exception {

        final String cmd = parserContext.getCurrentLine();
        if (!cmd.startsWith("no interface ")) {
            return;
        }

        // Get interface service-policy name
        String path = parserContext.getNCSCurrentKP(owner.device_id)+"/service-policy/"+metaData.argument;
        final String name = owner.maapiGetLeafString(worker, fromT, path);
        if (name == null) {
            return;
        }

        // Check if service-policy is added (on another interface) in same trans
        final String servicePolicy = " service-policy "+metaData.argument+" "+name;
        if (!owner.outputData.contains("\n"+servicePolicy+"\n")) {
            return;
        }

        // Pre-inject the interface service-policy delete
        String inject = cmd.substring(3)+"\n no"+servicePolicy+"\nexit\n";
        if (owner.extInjectFirst.indexOf(inject) < 0) {
            traceInfo(worker, "transformed => pre-injected "+stringQuote(inject));
            owner.extInjectFirst.append(inject);
        }
    }


    /**
     * "track-remove-before-change" - remove old track if type is changed
     * @param
     * @throws Exception
     */
    public void trackRemoveBeforeChange(final NedWorker worker, Schema.CallbackMetaData metaData,
                                        Schema.ParserContext parserContext, final int fromT, final int toT)
        throws Exception {

        final String cmd = parserContext.getCurrentLine();
        if (cmd.startsWith("no track ")) {
            return;
        }
        final String toType = getMatch(cmd, "track \\d+ (\\S+)");

        // Get previous track config
        String path = parserContext.getNCSCurrentKP(owner.device_id);
        if (!owner.maapiExists(worker, fromT, path)) {
            return;
        }
        final String from = owner.maapiGetConfig(worker, fromT, path, 0);
        final String fromType = getMatch(from, "track \\d+ (\\S+)");

        if (fromType.equals(toType)) {
            return;
        }

        final String inject = "no "+ getMatch(from, "(track \\d+ .+)");
        traceInfo(worker, "transformed => pre-injected "+stringQuote(inject));
        parserContext.injectBefore(inject);
    }


    /**
     * "prune-leaf-list-duplicates"
     * @param
     */
    public void pruneLeafListDuplicates(NedWorker worker, Schema.CallbackMetaData metaData, Schema.ParserContext parserContext) {
        List<Schema.TreeNode> leafListSeq = ((Schema.TreeSequence)parserContext.currentDataContext).getSequence();
        final String path = parserContext.currentDataContext.getKeyPath();
        Set<String> values = new HashSet<>();
        List<Schema.TreeNode> toBeRemoved = new ArrayList<>();
        for (Schema.TreeNode n : leafListSeq) {
            String value = ((Schema.TreeLeaf)n).getValue();
            if (values.contains(value)) {
                traceInfo(worker, "transformed => stripped duplicate "+value+" in "+shortpath(path));
                toBeRemoved.add(n);
            } else {
                values.add(value);
            }
        }
        leafListSeq.removeAll(toBeRemoved);
    }


    /*
     **************************************************************************
     * Common utility methods
     **************************************************************************
     */

    private String toEnter(Schema.ParserContext parserContext ) {
        StringBuilder sb = new StringBuilder();
        List<String> lines = parserContext.getEnterContextLines();
        for (String line : lines) {
            sb.append(line.replace("\r","")+"\n");
        }
        return sb.toString();
    }

    private String toExit(Schema.ParserContext parserContext ) {
        List<String> lines = parserContext.getEnterContextLines();
        if (lines.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder exit = new StringBuilder("exit\n");
        Iterator it = lines.iterator();
        while (it.hasNext()) {
            it.next();
            sb.insert(0, exit.toString());
            exit.insert(0, " ");
        }
        return sb.toString();
    }

    private void traceInfo(NedWorker worker, String info) {
        owner.traceInfo(worker, info);
    }
    private void traceVerbose(NedWorker worker, String info) {
        owner.traceVerbose(worker, info);
    }
    private void traceDebug2(NedWorker worker, String info) {
        owner.traceDebug(worker, info);
    }

    private String shortpath(String path) {
        return path.substring(path.indexOf("/config/")+8);
    }

}
